local filesystem = require('gears.filesystem')
local config_dir = filesystem.get_configuration_dir()
local utils_dir = config_dir .. 'utilities/'

return {
	-- The default applications that we will use in keybindings and widgets
	default = {
		-- Default terminal emulator
		terminal = 'alacritty',
		-- Default web browser
		web_browser = 'qutebrowser',
		-- Default text editor
		text_editor = 'emacs',
		-- Default file manager
		file_manager = 'nemo',
		-- Default media player
		multimedia = 'mpv',
		-- Default game, can be a launcher like steam
		game = 'supertuxkart',
		-- Default graphics editor
		graphics = 'gimp',
		-- Default sandbox
		sandbox = 'virtualbox',
		-- Default IDE
		development = '',
		-- Default network manager
		network_manager = 'alacritty iwctl',
		-- Default bluetooth manager
		bluetooth_manager = 'blueman-manager',
		-- Default power manager
		power_manager = 'xfce4-power-manager',
		-- Default GUI package manager
		package_manager = 'pamac-manager',
		-- Default locker
		lock = 'awesome-client "awesome.emit_signal(\'module::lockscreen_show\')"',
		-- Default quake terminal
		quake = 'alacritty --name QuakeTerminal',
		-- Default rofi global menu
		rofi_global = 'dmenu_run -i -p " Run :"',
		-- Default app menu
		rofi_appmenu = 'dmenu_run -i -c -l 10 -p " Run : "',

		-- You can add more default applications here
	},

	-- List of apps to start once on start-up
	run_on_start_up = {

      "picom &",
      "sxhkd &",
      "kdeconnect-cli &",
      "/usr/bin/emacs --daemon &",
      "lxsession &",
      "~/.fehbg &",
      "xrandr --output HDMI-2 --auto --right-of HDMI-1",

		-- You can add more start-up applications here
	},

	-- List of binaries/shell scripts that will execute for a certain task
	utils = {
		-- Fullscreen screenshot
		full_screenshot = utils_dir .. 'snap full',
		-- Area screenshot
		area_screenshot = utils_dir .. 'snap area',
		-- Update profile picture
		update_profile  = utils_dir .. 'profile-image'
	}
}
